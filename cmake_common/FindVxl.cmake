# Confirm that Point Grey FlyCapture SDK is installed
#
# This module defines
# VXL_FOUND, If false, don't try to use VXL
# VXL_PATH where the VXL top level directory is (WIN32 only)
# VXL_INCLUDE_PATH where the VXL headers are
# VXL_LIB_PATH where the VXL libs are
IF(WIN32)
	FIND_PATH( VXL_PATH include/vxl/core/vxl_version.h
	"C:/Program Files/vxl/"
	)

	if( VXL_PATH )
		SET(VXL_INCLUDE_PATH ${VXL_PATH}/include/vxl)
		SET(VXL_LIB_PATH ${VXL_PATH}/lib)
		
		message( STATUS "Looking for vxl - found")
		MESSAGE( STATUS "Vxl path: " ${VXL_PATH})
		SET ( VXL_FOUND 1 )
		
		INCLUDE_DIRECTORIES( ${VXL_INCLUDE_PATH}/core ${VXL_INCLUDE_PATH}/core/vil  ${VXL_INCLUDE_PATH}/vcl)
		
	else( VXL_PATH )
		message( STATUS "Looking for vxl  - not found")
		SET ( VXL_FOUND 0 )
	endif( VXL_PATH )
ELSE(WIN32)
	FIND_PATH( VXL_INCLUDE_PATH core
	# installation selected by user
	$ENV{VXL_HOME}/include
	# system placed in /usr/include
	/usr/include/vxl/
	# system placed in /usr/local/include
	/usr/local/include/vxl/
	)
	if( VXL_INCLUDE_PATH )
		message( STATUS "Looking for vxl - found")
		MESSAGE( STATUS "Vxl include path: " ${VXL_INCLUDE_PATH})
		SET ( VXL_FOUND 1 )
		INCLUDE_DIRECTORIES( ${VXL_INCLUDE_PATH}/core ${VXL_INCLUDE_PATH}/core/vil  ${VXL_INCLUDE_PATH}/vcl)
	else( VXL_INCLUDE_PATH )
		message( STATUS "Looking for vxl  - not found")
		SET ( VXL_FOUND 0 )
	endif( VXL_INCLUDE_PATH )
ENDIF(WIN32)
