/*------------------------------------------------------------------------
Copyright 2012 Michael Warren [michael.warren@qut.edu.au]

This file is part of MultiCameraDataPlayer

MultiCameraDataPlayer is free software: you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by the Free Software 
Foundation, either version 3 of the License, or (at your option) any later 
version.

MultiCameraDataPlayer is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
details.

You should have received a copy of the GNU General Public License along with 
MultiCameraDataPlayer. If not, see http://www.gnu.org/licenses/.
------------------------------------------------------------------------*/

// C++ Standard headers
#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <ctype.h>
#include <iomanip>
#include <signal.h>
#include <sstream>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>

#ifdef UNIX
	#include <unistd.h>
#endif //UNIX

// OpenCV Configuration and headers
#include "config.h"
#ifdef USE_OPENCV_1
    #include "cv.h"
    #include "highgui.h"
#else
    #include "imgproc/imgproc_c.h"
    #include "highgui/highgui_c.h"
#endif

//mkdir for visual studio
#ifdef _MSC_VER
	#include <direct.h>
#endif

// VXL Headers
#include <vcl_fstream.h>
#include <vul/vul_arg.h>
#include <vul/vul_awk.h>
#include <vul/vul_file_iterator.h>

using namespace std;

int createOutput = 0;
CvVideoWriter *writer1 = 0;

void sighandler(int sig)
{
    if (createOutput) {
        cvReleaseVideoWriter(&writer1);
        cout << "Releasing Video Writer" << endl;
    }
    exit(0);
}

void 
cvtToBgr( const IplImage* src, IplImage* dest, const std::string format )
{
    if( format == "BGR8" )
    {
        // do nothing but copy data into destination array.
        // Need to do the copy just in case the src image is being manipulated
        // and we don't want those manipulations to occur on the destination image
        // as well
        cvCopy(src, dest);

    }
    else if( format == "RGB8" )
    {
        cvCvtColor( src, dest, CV_RGB2BGR );
    }
    else if( format == "BGRA8" )
    {
        cvCvtColor( src, dest, CV_BGRA2BGR );
    }
    else if( format == "RGBA8" )
    {
        cvCvtColor( src, dest, CV_BGRA2BGR );
    }
    else if( format == "BayerRG8" )
    {
        cvCvtColor( src, dest, CV_BayerRG2BGR );
    }
    else if( format == "BayerGR8" )
    {
        cvCvtColor( src, dest, CV_BayerGR2BGR );
    }
    else if( format == "BayerGB8" )
    {
        cvCvtColor( src, dest, CV_BayerGB2BGR );
    }
    else if( format == "BayerBG8" )
    {
        cvCvtColor( src, dest, CV_BayerBG2BGR );
    }
    else if( format == "GRAY8" )
    {
        cvCvtColor( src, dest, CV_GRAY2BGR );
    }
    else
    {
        cout << "ERROR(colourconversions.cpp): Don't know how to convert image format into BGR8." << endl;
        //TODO: throw an exception
        exit(1);
    }
}


// Main Function
int main(int argc, char* argv[])
{
    signal(SIGINT, &sighandler);
    signal(SIGABRT, &sighandler);
    signal(SIGTERM, &sighandler);
    // Playback options
    bool forward_one_frame = false;
    bool backward_one_frame = false;


    // Camera and image input settings
    vul_arg<unsigned> in_num("-n", "number of cameras", 1);
    vul_arg<unsigned> in_cam("-c", "camera index to start from",0);
    vul_arg<vcl_string> in_out("-out", "output directory", "./");
    vul_arg<vcl_string> in_in("-in", "input directory", "./");
    vul_arg<vcl_string> in_name("-in_name", "input naming format, use regular expressions to seperate and a comma to seperate streams", "");
    vul_arg<double> in_framerate("-f", "playback frame rate", 200);
    vul_arg<vcl_string> file_list("-list", "Input file list, different lists are sperated by commas", "");
    // Video output settings
    vul_arg<unsigned> video_width("-video_width", "width of images used in video", 0);
    vul_arg<unsigned> video_height("-video_height", "height of images used in video", 0);
    vul_arg<double> video_framerate("-video_fps", "video file frame rate", 7.5);
    vul_arg<bool> in_combined_output_video("-output_combined_video", "output a combined video flag", false);
    vul_arg<bool> in_output_video("-output_video", "output video for each camera stream flag", false);
    vul_arg<vcl_string> in_codec("-out_codec", "video output codec", "MPEG");
    // Image output settings
    vul_arg<bool> in_output_image("-output_images", "output images flag", false);
    vul_arg<vcl_string> in_format("-in_format", "input format, images or video", "bmp");
    vul_arg<vcl_string> out_format("-out_format", "image output format", "jpg");
    vul_arg<bool> in_overwrite("-overwrite", "over-write images in a directory if it already exists", false);
    vul_arg<int> in_outnum("-out_num", "Output number for a single camera input",-1);
    // Input settings
    vul_arg<vcl_string> in_bayer("-bayer", "input image bayer format", "");
    vul_arg<unsigned> in_start("-start", "input start frame", 0);
    vul_arg<unsigned> in_end("-end", "input end frame", 100000000);
    vul_arg<vcl_string> in_invert("-invert", "invert image i", "");
    vul_arg<int> in_displace("-displace", "displacement", 0);
    vul_arg<vcl_string> in_undistort("-undistort", "folder location of undistortion parameters", "");
    vul_arg<unsigned> in_stride("-stride", "number of images to skip per iteration", 1);
    vul_arg<vcl_string> in_swap("-swap", "swap cameras that have their images the wrong way around", "");
    // GUI settings
    vul_arg<bool> in_pause("-pause", "pause_video_on_startup", false);
    vul_arg<bool> in_vertical("-vertical", "vertical or horizontal stereo", false);
    vul_arg<bool> in_visualise("-suppress", "suppress visualisation", false);
    //acid
    vul_arg<vcl_string> in_pname("-acid_project_name", "acid project name for outputting video or images", "");

    // parse the arguments
    vul_arg_parse(argc, argv);


    bool is_video = false;
    if (in_format() == "avi" || in_format() == "mpeg" || in_format() == "mpg" || in_format() == "MP4") {
        is_video = true;
    }
    bool createOutputImage = in_output_image();
    bool createOutputVideo = in_output_video();
    bool createCombinedOutputVideo = in_combined_output_video();
    unsigned num_cams = in_num();
    double framerate = in_framerate();
    std::vector<std::string> convertBayer(in_num(),"");
    // videowriters container when outputting video
    std::vector<CvVideoWriter*> videoWriters(in_num(),NULL);
    // input images directory
    string img_dir_string = in_in();
    // folder to put the output files in
    string out_string = in_out();
    // visualise the data in a vertical stereo configuration
    bool isVerticalStereo = in_vertical();
    // useful if you don't think the images are synchronised
    int displace = in_displace();
    // How many images to jump per update
    unsigned stride = in_stride();
    // number of cameras
    unsigned cam_num = in_cam();
    // Output camera ID for a single camera
    int out_num = in_outnum();
    // The size of the output video
    CvSize output_size = cvSize(video_width(),video_height());
   
    
    // Assign a camera swap vector
    std::vector<int> swap_vector;
    for (int ii = 0; ii < num_cams+cam_num; ii++) {
        swap_vector.push_back(ii); // Set each camera to it's defaults
    }
    
    // Assign a camera output vector
    std::vector<int> out_vector;
    for (int ii = 0; ii < num_cams; ii++) {
        out_vector.push_back(ii); // Set each camera to it's defaults
    }
    

    // Set the playback variable for pause/play on startup
    bool playing = !in_pause();

    // Set up bayer values
    if (in_bayer() != "") {
      std::string bayer_string = in_bayer();
      for (unsigned ii = 0; ii < bayer_string.size(); ii++) {
        char c = bayer_string.at(ii);
        std::string s(&c);
        int out;
        istringstream myStream(s);
        if (myStream>>out) {
          if (out < 9 && out >= 0) {
            std::string converted;
            switch (out) {
              case 0:
                converted.assign("GRAY8");
                cout << "Converting camera " << ii << " format to GRAY8" << endl;
                break;
              case 1:
                converted.assign("BayerBG8");
                cout << "Converting camera " << ii << " format to BayerBG8" << endl;
                break;
              case 2:
                converted.assign("BayerGB8");
                cout << "Converting camera " << ii << " format to BayerGB8" << endl;
                break;
              case 3:
                converted.assign("BayerGR8");
                cout << "Converting camera " << ii << " format to BayerGR8" << endl;
                break;
              case 4:
                converted.assign("BayerRG8");
                cout << "Converting camera " << ii << " format to BayerRG8" << endl;
                break;
              case 5:
                converted.assign("RGBA8");
                cout << "Converting camera " << ii << " format to RGBA8" << endl;
                break;
              case 6:
                converted.assign("BGRA8");
                cout << "Converting camera " << ii << " format to BayerBGRA8" << endl;
                break;
              case 7:
                converted.assign("RGB8");
                cout << "Converting camera " << ii << " format to RGB8" << endl;
                break;
              case 8:
              default:
                converted.assign("");
                break;
            }
            convertBayer[ii].assign(converted);
            cout << converted << endl;
          }
          else {
            cout << "Error, bayer value can only be in the range 0-9, you have provided " << out << endl;
          }
        } else {
          cout << "Error, could not convert " << myStream.str() << " to an integer" << endl;
        }
        cout << "Bayer Num: " << out << endl;
      }
    }
    cvResizeWindow("Output",640,480);
    vcl_vector<vcl_vector<vcl_string> > listnames;
    if (file_list() != "") {
        std::string in_list = file_list();
        unsigned end_idx = 0;
        unsigned start_idx = 0;

        while(end_idx < in_list.size()-1) {
            end_idx = in_list.find(',', start_idx);
            std::cout << "comma1: " << start_idx << ", comma2: " << end_idx << std::endl;
            if (end_idx > in_list.size())
                end_idx = in_list.size();
            // open the file
            std::string filename("0", end_idx - start_idx);
            for(unsigned ii=start_idx; ii < end_idx; ii++) {
                filename[ii - start_idx] = in_list[ii];
            }
            std::cout << "filename: [" << filename << "]" << std::endl;
	        vcl_ifstream spfs;
	        spfs.open(filename.c_str()); // open the file
	        if(!spfs.good()) {
		        std::cerr << "incorrect input file path: " << filename.c_str() << std::endl;
		        return false;
	        }
	        vul_awk ploader(spfs); // initialize
	        vcl_vector<string> templist;
	        // throw away the comments
			bool first = true;
	        for(;first || ploader; ++ploader) { // for each line
		        // skip the line if it is empty
		        if(!ploader.NF()) {
			        continue;
			    }
		        templist.push_back(ploader[0]);
				first = false;
		    }
		    listnames.push_back(templist);
            start_idx = end_idx+1;
        }
    }

    if (in_swap() != "") {
    // Need to allocate a non-standard swap vector
     std::string swap_string = in_swap();
      for (unsigned ii = 0; ii < swap_string.size(); ii++) {
        char c = swap_string.at(ii);
        std::string s(&c);
        int out;
        istringstream myStream(s);
        if (myStream>>out) {
          if (out < 9 && out >= 0) {
            swap_vector[ii] = out;
          }
          else {
            cout << "Error: swap value can only be 0-9. You have entered " << out << endl;
          }
        } else {
          cout << "Error: could not convert " << myStream.str() << " to an integer" << endl;
        }
        cout << "Setting camera " << ii << " swap id to " << swap_vector.at(ii) << endl;
      }
    }
    
    if (num_cams > 1 && in_outnum() != -1) {
        cout << "Error: You have entered the -in_outnum flag but there are more than 1 cameras in the input. Ignoring this flag. " << endl;
        out_num = -1;
    }
    

    std::vector<unsigned> invert;
    unsigned int dataCounter_ = in_start();
    int frameW = video_width();
    int frameH = video_height();
    bool vflag = !in_visualise();
    if (in_invert() != "") {
      std::string invert_string = in_invert();
      for (unsigned ii = 0; ii < invert_string.size(); ii++) {
        char c = invert_string.at(ii);
        std::string s(&c);
        int out;
        istringstream myStream(s);
        if (myStream>>out) {
          if (out < num_cams) {
            invert.push_back(out);
          }
        } else {
          cout << "Error, could not convert " << myStream.str() << " to an integer" << endl;
        }
        cout << "Num: " << out << endl;
      }
    }

    cvNamedWindow( "Output", 0 );
    if(!frameW || !frameH)
        cvResizeWindow("Output",640,480);
    else
        cvResizeWindow("Output",frameW,frameH);
    // Create output directory if desired
    if ((createOutputVideo || createCombinedOutputVideo || createOutputImage) && !(out_string == "." || out_string == img_dir_string)){
        // Make dir with RWX for owner/group RX for others
	//if (mkdir(out_string.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)) {
#ifdef _MSC_VER
		if (_mkdir(out_string.c_str())) {
#else
		if (mkdir(out_string.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)) {
#endif //WIN32
            if (in_overwrite()) {
                cout << "Warning: Over-writing files in output directory" << endl;
            } else {
                cout << "Could not create output directory" << endl;
                return -1;
            }
        }
        cout << "Output Directory: " << out_string << endl;
    }
    if (out_string == img_dir_string && (createOutputImage)) {
      cout << "Error: current configuration will overwrite images. Not continuing." << endl;
      exit(1);
    }
    if (createOutputImage && out_format().size() == 3) {
        cout << "output image format: " << out_format() << endl;
    }
    else if (createOutputImage) {
        cout << "cannot determine image format: " << out_format() << ". It must be only three characters, like \"bmp\" or \"jpeg\"." << endl;
        exit(1);
    }
    
    // Create image string containers
    std::vector<std::string> filenames(num_cams);
    // Create Image containers;
    std::vector<IplImage*> images(num_cams); 
    // create video input containers
    std::vector<CvCapture*> videos(num_cams, NULL);
    
    // Open up the video files if we are using videos
    if (is_video) {
        for (unsigned cam_index = 0; cam_index < num_cams; cam_index++) {
            std::string load_string;
            vcl_cout << "listnames size:" << listnames.size() << vcl_endl;
            if(listnames.size()) {
               load_string = in_in() + listnames[cam_index][dataCounter_];
               vcl_cout << load_string << vcl_endl;
            } else {
              stringstream ss;
              ss << "cam" << swap_vector[cam_index+cam_num] << "_video." << in_format();
              //ss << std::setw(4) << std::setfill('0') << dataCounter_ << "." <<in_format();
              filenames[cam_index] = ss.str();
              //std::cout << "img_dir_string: " << img_dir_string << std::endl;
              std::stringstream lss;
              lss << img_dir_string << filenames[cam_index];
              load_string = lss.str();
            }
            vcl_cout << "Creating file capture...";
            videos[cam_index] = cvCreateFileCapture(load_string.c_str());
            vcl_cout << "done" << vcl_endl;
            if (!videos[cam_index]) {
                vcl_cout << "Error! Could not open video file " << load_string << vcl_endl;
                exit(-1);
            }
        }
    }
    
    // Create a nice video for people to look at
    if (createCombinedOutputVideo) {
    
      std::string codec = in_codec();
      // Ensure video codec is correct
      if (codec.size() != 4) {
        cout << "Codec \"" << codec << "\" is not four letters!" << endl;
        exit(-1);
      }

      for(unsigned ii = 0; ii < codec.size(); ii++) {
        if (codec[ii] == '_')
          codec[ii] = ' ';
      }

      cout << "video codec: " << codec << endl;
      std::stringstream save_string_ss;
      save_string_ss << out_string << "/combined_" << codec << "_video.avi";
      std::string save_string = save_string_ss.str();
	  cout << "Output_video file: " << save_string << endl;
	  
      // allocate an image size for the video if not specified by the user by opening one of the images and extracting the default
      unsigned cam_index = 0;
      unsigned cam_num = 0;
      if (!output_size.width || !output_size.height) {
          if (!is_video) {
              std::string load_string;
              if(listnames.size()) {
                   load_string = in_in() + listnames[cam_index][dataCounter_];
              } else {
                  stringstream ss;
                  ss << "cam" << swap_vector[cam_index+cam_num] << "_image" << std::setw(5) << std::setfill('0') << dataCounter_ << "." << in_format();
                  //ss << std::setw(4) << std::setfill('0') << dataCounter_ << "." <<in_format();
                  filenames[cam_index] = ss.str();
                  //std::cout << "img_dir_string: " << img_dir_string << std::endl;
                  std::stringstream lss;
                  lss << img_dir_string << filenames[cam_index];
                  load_string = lss.str();
              }
              images[cam_index] = cvLoadImage(load_string.c_str(), -1);
              if(!images[cam_index]) {
                  cout << "Could not load image " << load_string << cam_index << endl;
                  if (writer1)
                     cvReleaseVideoWriter(&writer1);
                  for (unsigned ii = 0; ii < num_cams; ii++) {
                      if (videoWriters[ii]) {
                          cvReleaseVideoWriter(&videoWriters[ii]);
                      }
                  }
                  exit(-1);
               }
          } else {
              images[cam_index] = cvQueryFrame(videos[cam_index]);
              if(!images[cam_index]) {
                  cout << "Could not load frame from video " << cam_index << endl;
                  if (writer1)
                     cvReleaseVideoWriter(&writer1);
                  for (unsigned ii = 0; ii < num_cams; ii++) {
                      if (videoWriters[ii]) {
                          cvReleaseVideoWriter(&videoWriters[ii]);
                      }
                  }
                  exit(-1);
               }
              // reset the video back to the start
              if(cvSetCaptureProperty(videos[cam_index], CV_CAP_PROP_POS_FRAMES, 0)) {
                vcl_cout << "Error resetting back to start of video!" << vcl_endl;
                exit(-1);
              }
          }
          // allocate the correct size
          output_size = cvSize(images[cam_index]->width*num_cams,images[cam_index]->height);
      }
      
      writer1=cvCreateVideoWriter(save_string.c_str(),CV_FOURCC(codec[0], codec[1], codec[2], codec[3]),//0x00000000,//CV_FOURCC('P', 'I', 'M', '1'),//CV_FOURCC('D', 'I', 'V', 'X'), //CV_FOURCC_DEFAULT,
                            video_framerate(),output_size,1);
      cout << "created combined video: " << save_string.c_str() << endl;
    }
       
    // output the images to a video stream
    if (createOutputVideo) {
    
      std::string codec = in_codec();
      // Ensure video codec is correct
      if (codec.size() != 4) {
        cout << "Codec \"" << codec << "\" is not four letters!" << endl;
        exit(-1);
      }

      for(unsigned ii = 0; ii < codec.size(); ii++) {
        if (codec[ii] == '_')
          codec[ii] = ' ';
      }


      cout << "video codec: " << codec << endl;
      for (unsigned cam_index = 0; cam_index < num_cams; cam_index++) {
          std::stringstream save_string_ss;
          if (in_pname() != "") { // We are outputting an acid project
            save_string_ss << out_string << in_pname() << "." << std::setw(3) << std::setfill('0')
            << (out_num != -1 ? out_num : cam_index+cam_num) // If the out_num flag is set, force the camera number string to it.
            << ".avi";
          } else { // We are outputting a normal orca project
            save_string_ss << out_string << "cam" << cam_index+cam_num << "_" << codec << "_video.avi";
          }
          std::string save_string = save_string_ss.str();
	      cout << "Output_video file: " << save_string << endl;
	      
          // allocate an image size for the video if not specified by the user by opening one of the images and extracting the default
          if (!output_size.width || !output_size.height) {
              if (!is_video) {
                  std::string load_string;
                  if(listnames.size()) {
                       load_string = in_in() + listnames[cam_index][dataCounter_];
                  } else {
                      stringstream ss;
                      ss << "cam" << swap_vector[cam_index+cam_num] << "_image" << std::setw(5) << std::setfill('0') << dataCounter_ << "." << in_format();
                      //ss << std::setw(4) << std::setfill('0') << dataCounter_ << "." <<in_format();
                      filenames[cam_index] = ss.str();
                      //std::cout << "img_dir_string: " << img_dir_string << std::endl;
                      std::stringstream lss;
                      lss << img_dir_string << filenames[cam_index];
                      load_string = lss.str();
                  }
                  images[cam_index] = cvLoadImage(load_string.c_str(), -1);
                  if(!images[cam_index]) {
                      cout << "Could not load image " << load_string << endl;
                      if (writer1)
                         cvReleaseVideoWriter(&writer1);
                      for (unsigned ii = 0; ii < num_cams; ii++) {
                          if (videoWriters[ii]) {
                              cvReleaseVideoWriter(&videoWriters[ii]);
                          }
                      }
                      exit(-1);
                   }
              } else {
                  images[cam_index] = cvQueryFrame(videos[cam_index]);
                  if(!images[cam_index]) {
                      cout << "Could not load frame from video " << cam_index << endl;
                      if (writer1)
                         cvReleaseVideoWriter(&writer1);
                      for (unsigned ii = 0; ii < num_cams; ii++) {
                          if (videoWriters[ii]) {
                              cvReleaseVideoWriter(&videoWriters[ii]);
                          }
                      }
                      exit(-1);
                   }
                  // reset the video back to the start
                  if(cvSetCaptureProperty(videos[cam_index], CV_CAP_PROP_POS_FRAMES, 0)) {
                    vcl_cout << "Error resetting back to start of video!" << vcl_endl;
                    exit(-1);
                  }
            }
            // allocate the correct size
            output_size = cvSize(images[cam_index]->width,images[cam_index]->height);
          }
          videoWriters[cam_index]=cvCreateVideoWriter(save_string.c_str(),CV_FOURCC(codec[0], codec[1], codec[2], codec[3]),//0x00000000,//CV_FOURCC('P', 'I', 'M', '1'),//CV_FOURCC('D', 'I', 'V', 'X'), //CV_FOURCC_DEFAULT,
                                video_framerate(),output_size,1);
          if(!videoWriters[cam_index]) {
              cout << "Could not make video writer! " << cam_index << endl;
              if (writer1)
                 cvReleaseVideoWriter(&writer1);
              for (unsigned ii = 0; ii < num_cams; ii++) {
                  if (videoWriters[ii]) {
                      cvReleaseVideoWriter(&videoWriters[ii]);
                  }
              }
              exit(-1);
           }
          cout << "created video: \"" << save_string <<"\"" << endl;
      }
    }
    
    
    // Load the intrinsics and undostortion matrices
    std::vector<CvMat*> intrinsics(num_cams);
    std::vector<CvMat*> distortions(num_cams);
    
    if (in_undistort() != "") {
          // Make sure user has entered all the necessary parameters
            for (int cam_index = 0; cam_index < num_cams; cam_index++) {
              // Load the intrinsic matrix
              std::stringstream intrinsics_s;
              intrinsics_s << in_undistort() << "cam" << cam_index+cam_num << "_intrinsics.xml";
              //std::cout << intrinsics_s.str() << std::endl;
              vcl_string intrinsics_string(intrinsics_s.str());
              intrinsics[cam_index] = (CvMat*) cvLoad(intrinsics_string.c_str());
              if (!intrinsics[cam_index]) {
                  vcl_cout << "Error! Could not load intrinsics properly!" << vcl_endl;
              }
              // Load the distortion matrix
              std::stringstream distortion_s;
              distortion_s << in_undistort() << "cam" << cam_index+cam_num << "_distortion.xml";
              //std::cout << distortion_s.str() << std::endl;
              vcl_string distortion_string(distortion_s.str());
              distortions[cam_index] = (CvMat*) cvLoad(distortion_string.c_str());
              if (!distortions[cam_index]) {
                  vcl_cout << "Error! Could not load distortion parameters properly!" << vcl_endl;
              }
          }
      }
    
    

    ////////////////////////////////////////////////// Main Loop
    while(1) {
        // Create image strings normally
        bool load_check = true;
        for (int cam_index = 0; cam_index < num_cams; cam_index++) {
          if (!is_video) { // if not video
              std::string load_string;
              if(listnames.size()) {
                   load_string = in_in() + listnames[cam_index][dataCounter_];
              } else {
                  stringstream ss;
                  ss << "cam" << swap_vector[cam_index+cam_num] << "_image" << std::setw(5) << std::setfill('0') << dataCounter_ << "." << in_format();
                  filenames[cam_index] = ss.str();
                  std::stringstream lss;
                  lss << img_dir_string << filenames[cam_index];
                  load_string = lss.str();
              }
              images[cam_index] = cvLoadImage(load_string.c_str(), -1);
              if(!images[cam_index]) {
                cout << "Could not load image file: " << load_string << endl;
                load_check = false;
                //continue;
                if (writer1)
                  cvReleaseVideoWriter(&writer1);
                for (unsigned ii = 0; ii < num_cams; ii++) {
                    if (videoWriters[ii]) {
                        cvReleaseVideoWriter(&videoWriters[ii]);
                    }
                }
                exit(-1);
              }
          } else { // is video
              for(unsigned ii = 0; ii < in_stride(); ii++) {
                images[cam_index] = cvQueryFrame(videos[cam_index]);
              }
              if(!images[cam_index]) {
                cout << "Could not load image from video " << cam_index << endl;
                load_check = false;
                //continue;
                if (writer1)
                  cvReleaseVideoWriter(&writer1);
                for (unsigned ii = 0; ii < num_cams; ii++) {
                    if (videoWriters[ii]) {
                    	cout << "Releasing Video Writer" << endl;
                        cvReleaseVideoWriter(&videoWriters[ii]);
                    }
                }
                exit(-1);
              }
          }
        }
        if (!load_check) {
            dataCounter_ += stride; // Increment the image counter    
            for (int cam_index = 0; cam_index < num_cams; cam_index++)
                if (!images.at(cam_index))
                    cvReleaseImage(&images.at(cam_index));
            continue;
        }       
        CvSize imageSize = cvGetSize(images[0]);
        // We only work with 8 bit images. If something else, convert it
        for (unsigned cam_index = 0; cam_index < num_cams; cam_index++) {
            if(images[cam_index]->depth > 8) {
                  //std::cout << "making converted imag" << std::endl;
                  IplImage* cvted = cvCreateImage(imageSize, IPL_DEPTH_8U, images[cam_index]->nChannels);
                  //cvConvertImage( cap_img1, flipped, CV_CVTIMG_FLIP );
                  cvConvertScale(images[cam_index], cvted, 1.0/16);
                  cvReleaseImage(&images[cam_index]);
                  images[cam_index] = cvCloneImage(cvted);
                  cvReleaseImage(&cvted);
                  //std::cout << "finished making converted imag" << std::endl;
            }
        }
        // Invert images as appropriate
        if (invert.size()) {
            for (unsigned invert_index = 0, cam_index = invert[invert_index]; invert_index < invert.size(); invert_index++, cam_index = invert[invert_index]) {
              // Flip image
              IplImage* flipped = cvCreateImage(imageSize, images[cam_index]->depth, images[cam_index]->nChannels);
              //cvConvertImage( cap_img1, flipped, CV_CVTIMG_FLIP );
              cvFlip( images[cam_index], flipped, -1);
              cvReleaseImage(&images[cam_index]);
              images[cam_index] = cvCloneImage(flipped);
              cvReleaseImage(&flipped);
            }
        }
        // Convert to colour as appropriate
        for (int cam_index = 0; cam_index < num_cams; cam_index++) {
          if (convertBayer[cam_index] != "") {
              if (images[cam_index]->nChannels != 1) {
                cout << "Error: Images are already colour, cannot convert bayer encoding." << endl;
                exit(1);
              }
              //IplImage* grayscale_frame = cvCreateImage( cvSize(images.at(cam_index)->width, images.at(cam_index)->height), 8, 1 );
              IplImage* colour_frame = cvCreateImage( cvSize(images.at(cam_index)->width, images.at(cam_index)->height), 8, 3 );
              cvtToBgr(images.at(cam_index), colour_frame, convertBayer.at(cam_index));
              cvReleaseImage(&images[cam_index]);
              images[cam_index] = colour_frame;
          }
        }
        // Undistort
        if (in_undistort() != "") {
          // Make sure user has entered all the necessary parameters
            for (int cam_index = 0; cam_index < num_cams; cam_index++) {
              IplImage* temp_frame = cvCreateImage( cvSize(images.at(cam_index)->width, images.at(cam_index)->height), 8, images[cam_index]->nChannels );
              cvUndistort2(images[cam_index], temp_frame, intrinsics[cam_index], distortions[cam_index]);
              //vcl_cout << "done" << vcl_endl;
              if(!is_video) {
                cvReleaseImage(&images[cam_index]);
              }
              images[cam_index] = temp_frame;
              //vcl_cout << "done undistorting " << vcl_endl;
          }
        }

        // Write to combined video if selected
        if (createCombinedOutputVideo) {
            IplImage* writer_frame = cvCreateImage( cvSize(imageSize.width*num_cams, imageSize.height), 8, 3 );
            vector<IplImage*> colour_frames(num_cams);
            for (int cam_index = 0; cam_index < num_cams; cam_index++) {
              if (images[cam_index]->nChannels == 1) {
                IplImage* colour_frame = cvCreateImage( cvSize(images.at(cam_index)->width, images.at(cam_index)->height), 8, 3 );
                cvtToBgr(images.at(cam_index), colour_frame, "GRAY8");
              }
              else {
                  colour_frames[cam_index] = images[cam_index];
              }
              cvSetImageROI( writer_frame, cvRect( imageSize.width*cam_index, 0, imageSize.width*(cam_index+1), writer_frame->height ) );
#ifdef USE_OPENCV_1
              cvCopyImage( colour_frames[cam_index], writer_frame);
#else
              cvCopy( colour_frames[cam_index], writer_frame);
#endif            
            }
            cvResetImageROI( writer_frame );
            cvWriteFrame(writer1,writer_frame);
            cvReleaseImage(&writer_frame);
            for (int cam_index = 0; cam_index < num_cams; cam_index++) {
              if (images[cam_index]->nChannels == 1) {
                cvReleaseImage(&colour_frames[cam_index]);
              } 
            }
        }

        // Write to video files if selected
        if (createOutputVideo) {
            vector<IplImage*> colour_frames(num_cams);
            for (int cam_index = 0; cam_index < num_cams; cam_index++) {
                if (images[cam_index]->nChannels == 1) {
                    colour_frames[cam_index] = cvCreateImage( cvSize(images.at(cam_index)->width, images.at(cam_index)->height), 8, 3 );
                    cvtToBgr(images.at(cam_index), colour_frames[cam_index], "GRAY8");
                }
                else {
                    colour_frames[cam_index] = images[cam_index];
                }
            }
            for (unsigned cam_index = 0; cam_index < num_cams; cam_index++) {
              cvWriteFrame(videoWriters[cam_index],colour_frames.at(cam_index));
            }
            for (int cam_index = 0; cam_index < num_cams; cam_index++) {
              if (images[cam_index]->nChannels == 1) {
                cvReleaseImage(&colour_frames[cam_index]);
              } 
            }
        }

        // Write to images if selected
        if (createOutputImage) {
            for (int cam_index = 0; cam_index < num_cams; cam_index++) {
              stringstream ss;
              if (in_pname() != "") { // We are outputting an acid project
                ss << out_string << "/" << in_pname() << "." << std::setw(3) 
                << std::setfill('0') << cam_index << "." << std::setw(5) 
                << std::setfill('0') << dataCounter_ << "." << out_format();
              } else  { // We are outputting a normal orca project
                ss << out_string << "/" << "cam"
                << (out_num != -1 ? out_num : cam_index + in_cam()) // If the out_num flag is set, force the camera number string to it.
                << "_image" 
                << std::setw(5) << std::setfill('0') << dataCounter_ << "." << out_format();
              }
              
              std::string save_string = ss.str();
              cvSaveImage(save_string.c_str(), images.at(cam_index));
            }
        }

        // Write filenames on images
        CvFont font;
        double hScale=0.7;
        double vScale=0.7;
        int    lineWidth=1;
        cvInitFont(&font,CV_FONT_HERSHEY_SIMPLEX|CV_FONT_ITALIC, hScale,vScale,0,lineWidth);
        for (int cam_index = 0; cam_index < num_cams; cam_index++) {
          cvPutText(images[cam_index],filenames[cam_index].c_str(),cvPoint(10,25), &font, cvScalar(255,255,0));
        }

        IplImage *visualise;
        if (!isVerticalStereo) { // Horizontal images
            visualise = cvCreateImage( cvSize(imageSize.width*num_cams, imageSize.height), 8, 3);
            vector<IplImage*> colour_frames(num_cams);
            for (int cam_index = 0; cam_index < num_cams; cam_index++) {
                if (images[cam_index]->nChannels == 1) {
                    colour_frames[cam_index] = cvCreateImage( cvSize(images.at(cam_index)->width, images.at(cam_index)->height), 8, 3 );
                    cvtToBgr(images.at(cam_index), colour_frames[cam_index], "GRAY8");
                }
                else {
                    colour_frames[cam_index] = images[cam_index];
                }
            }
            int corner_1_x = 0;
            int corner_1_y = 0;
            int corner_2_x = imageSize.width;
            int corner_2_y = imageSize.height;
            for (int index = 0; index < images.size(); index++) {
                cvSetImageROI( visualise, cvRect( corner_1_x, corner_1_y, imageSize.width, imageSize.height ) );
                //cvCopy( images_colour[index], visualise );
                cvResize(colour_frames[index],visualise,CV_INTER_LINEAR);
                cvResetImageROI( visualise );
                corner_1_x = corner_2_x;
                corner_2_x += imageSize.width;
            }
            cvResetImageROI( visualise );
            for (int cam_index = 0; cam_index < num_cams; cam_index++) {
              if (images[cam_index]->nChannels == 1) {
                cvReleaseImage(&colour_frames[cam_index]);
              } 
            }
        }
        else { // Vertical images
            visualise = cvCreateImage( cvSize(imageSize.width, imageSize.height*num_cams), 8, images[0]->nChannels );
            int corner_1_x = 0;
            int corner_1_y = 0;
            int corner_2_x = imageSize.width;
            int corner_2_y = imageSize.height;
            vector<IplImage*> colour_frames(num_cams);
            for (int cam_index = 0; cam_index < num_cams; cam_index++) {
                if (images[cam_index]->nChannels == 1) {
                    colour_frames[cam_index] = cvCreateImage( cvSize(images.at(cam_index)->width, images.at(cam_index)->height), 8, 3 );
                    cvtToBgr(images.at(cam_index), colour_frames[cam_index], "GRAY8");
                }
                else {
                    colour_frames[cam_index] = images[cam_index];
                }
            }
            for (int index = 0; index < images.size(); index++) {
                cvSetImageROI( visualise, cvRect( corner_1_x, corner_1_y, corner_2_x, corner_2_y ) );
                //cvCopy( images_colour[index], visualise );
                cvResize(colour_frames[index],visualise,CV_INTER_LINEAR);
                cvResetImageROI( visualise );
                corner_1_y = corner_2_y;
                corner_2_y += imageSize.height;
            }
            cvResetImageROI( visualise );
            for (int cam_index = 0; cam_index < num_cams; cam_index++) {
              if (images[cam_index]->nChannels == 1) {
                cvReleaseImage(&colour_frames[cam_index]);
              } 
            }
        }
        
        // If we want to view the data
        if (vflag) {
            if (!cvGetWindowHandle( "Output")) {
                //User has closed the window
                if (createOutput) cvReleaseVideoWriter(&writer1);
                exit(0);
            }
            cvShowImage("Output", visualise);
            if (!isVerticalStereo)
                cvResizeWindow("Output",640*num_cams,480);
            else
                cvResizeWindow("Output",640,480*num_cams);
            int waitTime = (1.0/framerate)*1000;
            char c;
            if (playing == true) {
                c = cvWaitKey(waitTime);
            }
            else {
                c = cvWaitKey(0);
            }
            if( c == 27 ) {
                cvDestroyWindow( "Output");
                cvReleaseVideoWriter(&writer1);
                for (unsigned cam_index = 0; cam_index < num_cams; cam_index++) {
                    if (videoWriters[cam_index]) {
                        cvReleaseVideoWriter(&(videoWriters[cam_index]));
                    }
                }
                exit(0);
            }
            else if (c == 32 && playing == true) {
                playing = false;
            }
            else if (c == 32 && playing == false) {
                playing = true;
            }
            else if (c == 110) { //n
                playing = false;
                dataCounter_-= 2*stride;
            }
            else if (c == 109) { //m
                playing = false;
            }
        }
        cvReleaseImage(&visualise);
        if(!is_video || in_undistort() != "") {
            for (int cam_index = 0; cam_index < num_cams; cam_index++)
              cvReleaseImage(&images.at(cam_index));
        }
        dataCounter_ += stride; // Increment the image counter
        if (dataCounter_ > in_end()) {
          cout << "Reached last desired frame. Exiting" << endl;
          if (writer1) {
            cout << "Releasing Video Writer" << endl;
            cvReleaseVideoWriter(&writer1);
          }
          for (int cam_index = 0; cam_index < num_cams; cam_index++) {
            if (videoWriters[cam_index])
              cout << "Releasing Video Writer " << cam_index << endl;
              cvReleaseVideoWriter(&videoWriters[cam_index]);
            }
          
          exit(0);
        }
    }
}
